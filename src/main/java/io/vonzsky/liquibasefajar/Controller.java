package io.vonzsky.liquibasefajar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("person")
public class Controller {

    @Autowired
    private PersonRepository personRepository;
    @PostMapping("person")
    public String cratePerson(@RequestParam String name){
        personRepository.save(new Person(name, "6.8"));
        return personRepository.findByName(name) + " saved";
    }

    @GetMapping("person")
    public List<Person> getAllThePeople(){
        return (List<Person>) personRepository.findAll();
    }
}
