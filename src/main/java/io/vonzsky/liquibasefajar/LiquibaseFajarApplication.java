package io.vonzsky.liquibasefajar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LiquibaseFajarApplication {

    public static void main(String[] args) {
        SpringApplication.run(LiquibaseFajarApplication.class, args);
    }

}
